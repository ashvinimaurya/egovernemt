import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

import Flexi from './component/Flexi/Flexi';

class App extends Component {

  state = {
    name: '',
    state: ''
  }

  onChangeHandler = (e) => {
    this.setState({
      name: e.target.value
    })
  }

  onStateChangeHandler = (e) => {
    this.setState({
      state: e.target.value
    })
  }

  onFlexiSubmit = (e) => {
    e.preventDefault();
    alert("Submitted Name and State: " + this.state.name + " " + this.state.state)
  }

  render() {
    const flexiConfig = {
      items: [
        {
          name: "person_name",
          label: "Person's Name",
          type: "TextField"
        },
        {
          name: "states",
          label: "Person's state",
          type: "DropDown",
          values: [
            "Maharashtra",
            "Kerala",
            "Tamil Nadu"
          ]
        }
      ]
    };

    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
        </header>

        <Flexi 
          onSubmit={this.onFlexiSubmit} 
          config={flexiConfig}
          onChange={this.onChangeHandler} 
          onStateChange={this.onStateChangeHandler} />
      </div>
    );
  }
}

export default App;
