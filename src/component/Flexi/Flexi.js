import React, { Component } from 'react';

class Flexi extends Component {
  render() {
    const inputArray = [];
    let inputElement = null;

    this.props.config.items.forEach(obj => {
      if (obj.type === "TextField") {
        inputElement = <input type="text" name="name" placeholder="Enter your name" onChange={this.props.onChange} />
        inputArray.push(inputElement);
      }

      if (obj.type === "DropDown") {
        inputElement = (
          <select name="state" onChange={this.props.onStateChange}>
            <option value="Maharashtra">Maharashtra</option>
            <option value="Kerala">Kerala</option>
            <option value="Tamil Nadu">Tamil Nadu</option>
          </select>
        )
        inputArray.push(inputElement);
      }
    });

    return (
      <form onSubmit={this.props.onSubmit}>
        {inputArray.map((inputField, index) => (
          <div key={index}> {inputField} </div>
        ))}
        <button>Submit</button>
      </form>
    );
  }
}

export default Flexi;
