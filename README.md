# eGovernment
* The project is deployed by [surge](http://surge.sh/)
* Live app: [eGovernment](http://egovernment.surge.sh/)

This project is developed using Reactjs.

Here are the steps which I followed during the development.

  - `create-react-app` is used to create the boilerplate of the app
  - `form inputs` are created depending on flexiConfig data

